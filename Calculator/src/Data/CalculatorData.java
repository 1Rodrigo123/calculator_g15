package Data;

public class CalculatorData {
    static public String calculate(double num1, double num2, String operator) {
        double ret;
        try {
            ret = switch (operator) {
                case "+" -> _add(num1, num2);
                case "X" -> _multiply(num1, num2);
                case "-" -> _subtract(num1, num2);
                case "/" -> _divide(num1, num2);
                default -> throw new Exception();
            };
        } catch (ArithmeticException e) {
            return e.getMessage();
        } catch (Exception e) {
            return "ERROR";
        }
        return formatNumbers(ret);
    }

    static private double _add(double num1, double num2) {
        return num1 + num2;
    }

    static private double _subtract(double num1, double num2) {
        return num1 - num2;
    }

    static private double _divide(double num1, double num2) throws ArithmeticException {
        if (num2 == 0)
            throw new ArithmeticException("Impossível dividir por 0");
        return num1 / num2;
    }

    static private double _multiply(double num1, double num2) {
        return num1 * num2;
    }

    static public double potencia(double num1) {
        return num1 * num1;
    }

    static public String decimalToBinary(int num) {
        // negativos?
        if (num == 0) {
            return "0";
        }
        StringBuilder binary = new StringBuilder();
        while (num > 0) {
            int resto = num % 2;
            binary.insert(0, resto);
            num = num / 2;
        }
        return binary.toString();
    }

    static public long fibonacci(int num){

        if (num <= 1) {
            return num;
        }
        return fibonacci(num - 1) + fibonacci(num - 2);
    }

    static public String factorial(int num) {
        if (num < 0) {
            return "Nao existe";
        }
        if (num == 0 || num == 1) {
            return "1";
        }

        long resultado = 1;
        for (int i = 2; i <= num; i++) {
            resultado *= i;
        }

        return Long.toString(resultado);
    }

    public static String formatNumbers(double num) {
        if (num == (int) num) {
            return Integer.toString((int) num);
        } else {
            return Double.toString(num);
        }
    }
}
