package UI;

import Data.CalculatorData;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MainJFX extends Application {
    private TextField tf = new TextField();
    private final String[] cmd = {"C", "«", "x^2", "/", "7", "8", "9", "X", "4", "5", "6",
                                    "-", "1", "2", "3", "+", "+/-", "0", ".", "=", "BIN","FIBO","!"};

    private final String title = "CALCULATOR - G15";
    private String op;
    private Boolean flag;

    private double num1, num2;
    private String result;
    private boolean clean;

    @Override
    public void start(Stage stage) throws Exception {
        tf.setFont(Font.font(20));
        tf.setAlignment(Pos.CENTER_RIGHT);
        tf.setPrefHeight(50);
        tf.setEditable(false);

        StackPane stackPane = new StackPane();
        stackPane.setPadding(new Insets(10));
        stackPane.getChildren().add(tf);

        TilePane tilePane = new TilePane();
        tilePane.setHgap(10);
        tilePane.setVgap(10);
        tilePane.setAlignment(Pos.TOP_CENTER);
        tilePane.setPadding(new Insets(10, 20, 10, 20));
        for (String str : cmd) {
            tilePane.getChildren().addAll(commandsButton(str));
        }

        BorderPane root = new BorderPane();
        root.setTop(stackPane);
        root.setCenter(tilePane);
        Scene scene = new Scene(root, 300, 450);
        stage.setScene(scene);
        stage.setTitle(title);
        stage.setResizable(false);
        stage.show();
        flag = false;
    }

    private void readNumber(ActionEvent e) {
        if (clean) {
            tf.setText("");
            clean = false;
        }
        String s = ((Button) e.getSource()).getText();
        tf.setText(tf.getText() + s);
    }

/*
    private void _readOperator(ActionEvent e) {
        String operator = ((Button) e.getSource()).getText();
        switch (operator) {
            case "=" -> {
                if (!op.isEmpty()) {
                    num2 = Double.parseDouble(tf.getText().substring(tf.getText().indexOf(op) + 1));
                    result = CalculatorData.calculate(num1, num2, op);
                    tf.setText(result);
                    op = "";
                    clean = true;
                    flag = false;
                }
            }
            case "x^2" -> {
                num1 = Double.parseDouble(tf.getText());
                result = CalculatorData.formatNumbers(CalculatorData.potencia(num1));
                tf.setText(result);
                op = "";
                clean = true;
                flag = false;
            }
            case "BIN" -> {
                num1 = Double.parseDouble(tf.getText());
                result = CalculatorData.decimalToBinary((int) num1);
                tf.setText(result);
                op = "";
                clean = true;
                flag = false;
            }
            default -> {
                if (op.isEmpty()) {
                    num1 = Double.parseDouble(tf.getText());
                    op = operator;
                    tf.setText(tf.getText() + operator);
                    flag = true;
                }
            }
        }
    }
*/
    private void readOperator(ActionEvent e) {
        String operator = ((Button) e.getSource()).getText();

        if (operator.equals("=")) {
            if (op.equals("x^2")) {
                result = Double.toString(CalculatorData.potencia(num1));
            } else if(op.equals("BIN")) {
                result = CalculatorData.decimalToBinary((int) num1);

            }else if (op.equals("!")) {
                if (num1 < 0 || num1 != (int) num1) {
                    result = "Nao existe";
                } else {
                    result = CalculatorData.factorial((int) num1);
                }
            } else if(op.equals("FIBO")){
                result = Long.toString(CalculatorData.fibonacci((int) num1));
            } else {
                num2 = Double.parseDouble(tf.getText().substring(tf.getText().indexOf(op) + 1));
                result = CalculatorData.calculate(num1, num2, op);
            }
            tf.setText(result);
            op = "";
            clean = true;
            flag = false;
        } else {
            num1 = Float.parseFloat(tf.getText());
            op = operator;
            tf.setText(tf.getText() + operator);
            flag = true;
            System.out.printf("OOo");
        }
    }

    private Button commandsButton(String s) {

        Button btn = new Button(s);
        btn.setFont(Font.font(15));
        btn.setPrefSize(50, 50);

        switch (s) {
            case "C":
                btn.setOnAction(e -> {
                    tf.setText("");
                });
                break;
            case "«":
                btn.setOnAction(e -> {
                    String s1 = tf.getText();
                    if (!s1.isEmpty()) {
                        s1 = s1.substring(0, s1.length() - 1);
                        tf.setText(s1);
                    }
                });
                break;
            case "+/-":
                btn.setOnAction(e -> {
                    if (!flag)
                        tf.setText("-" + tf.getText());
                });
                break;
            case "=":
                btn.setOnAction(this::readOperator);
                btn.setStyle("-fx-background-color: orange; -fx-border-color: orange;");
                break;

            case "+", "-", "/", "X", "x^2", "BIN","FIBO","!":
                btn.setOnAction(this::readOperator);
                break;
            default:
                btn.setOnAction(this::readNumber);
        }
        return btn;
    }
}
